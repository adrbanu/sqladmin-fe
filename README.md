SqlAdmin
======

The React version of SqlAdmin
Installation
------------

It requires nodejs with npm already installed on your computer.

After cloning the repo inside the project directory run:

	> npm install

Running the app in development mode:
----------

It includes a fake backend.

	> npm start

Generate a production build
-----------

It generates a **build** directory in the root of the project containing production ready files.

	> npm run build