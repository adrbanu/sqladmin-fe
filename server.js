let path = require('path');
let opn = require('opn');
let webpack = require('webpack');
let express = require('express');
let devMiddleware = require('webpack-dev-middleware');
let hotMiddleware = require('webpack-hot-middleware');

const PORT = 8000;
let app = express();

app.get('/databases/add', function (req, res) {
	res.send('{"abc": "def"}');
});

app.use(express.static(path.join(__dirname, 'src')));
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));

setupWebpack(app)
	.then(openServerAndApp)
	.catch(err => console.error(err));

function setupWebpack(app) {
	return new Promise(resolve => {
		let webpackConfig = require(path.join(__dirname, 'webpack-dev-config.js'));
		let compiler = webpack(webpackConfig);
		compiler.apply(new webpack.ProgressPlugin());
		let devMiddlewareInstance = devMiddleware(compiler, {
			publicPath: webpackConfig.output.publicPath,
			debug: true,
			stats: {
				colors: true,
				errors: true,
				errorDetails: true,
				publicPath: true
			}
		});
		app.use(devMiddlewareInstance);
		app.use(hotMiddleware(compiler));

		devMiddlewareInstance.waitUntilValid(resolve);
	});
}

function openServerAndApp() {
	app.listen(PORT, () => console.log(`\nlistening on PORT ${PORT}`));
	opn(`http://localhost:${PORT}/`);
}