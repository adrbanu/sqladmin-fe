let path = require('path');
let fs = require('fs');
let webpack = require('webpack');
let mime = require('mime');
let sass = require('node-sass');
let importOnce = require('node-sass-import-once');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let UglifyJsPlugin = require('uglifyjs-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
	mode: 'production',
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: '[hash].[name].js'
	},
    devtool: 'inline-source-map',
	performance: {
		hints: false
	},
	entry: [
		// './src/backend/backend',
		'./src/index.js'
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								sourceMap: false,
								url: false,
								minimize: true
							}
						},
						{
							loader: 'sass-loader',
							options: {
								importer: importOnce,
								includePaths: [
									path.resolve(__dirname, 'node_modules/compass-mixins/lib'),
									path.resolve(__dirname, 'node_modules'),
								],
								functions: {
									'inline-image($filename, $mime_type: null)': function(filename, mimeType, done) {
										done(sassInlineFile(filename, path.resolve(__dirname, 'src/assets/img')));
									}
								},
								sourceMap: false
							}
						}
					]
				})
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({'process.env.NODE_ENV': JSON.stringify('production')}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new CleanWebpackPlugin(['build']),
		// new UglifyJsPlugin({
		// 	exclude: /node_modules/,
		// 	cache: true,
		// 	parallel: true,
		// 	uglifyOptions: {
		// 		exclude: /node_modules/,
		// 		mangle: {
		// 			toplevel: false
		// 		},
		// 		toplevel: false,
		// 		warnings: false,
		// 		compress: {
		// 			'dead_code': true,
		// 			'warnings': false,
		// 			'drop_debugger': true,
		// 			'pure_funcs': [
		// 				'console.log',
		// 				'console.info',
		// 				'console.dir',
		// 				'console.group',
		// 				'console.groupCollapsed',
		// 				'console.groupEnd',
		// 				'console.table',
		// 				'console.time',
		// 				'console.timeEnd',
		// 				'console.trace'
		// 			]
		// 		}
		// 	}
		// }),
		new ExtractTextPlugin('[md5:contenthash:hex:20].[name].css'),
		new HtmlWebpackPlugin({
			hash: false,
			filename: 'index.html',
			template: 'template-index.html'
		})
	]
};

function sassInlineFile(filename, rootDir) {
	let sourceFilePath = filename.getValue();
	let fullPath = path.resolve(rootDir, sourceFilePath.startsWith('/') ? sourceFilePath.slice(1) : sourceFilePath);
	let mimetype = mime.getType(path.extname(fullPath).slice(1));
	let fontContent = fs.readFileSync(fullPath);
	return new sass.types.String(`url(data:${mimetype ? `${mimetype};` : ''}base64,${fontContent.toString('base64')})`);
}