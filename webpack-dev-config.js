let fs = require('fs');
let path = require('path');
let webpack = require('webpack');
let mime = require('mime');
let sass = require('node-sass');
let importOnce = require('node-sass-import-once');

module.exports = {
	mode: 'development',
	output: {
		path: path.resolve(__dirname, 'src'),
		publicPath: ``,
		filename: '[name]'
	},
	devtool: 'inline-source-map',
	performance: {
		hints: false
	},
	entry: {
		'index-bundle.js': [
			'webpack-hot-middleware/client?name=js&reload=true',
			'./src/backend/backend',
			'./src/index.js'
		]
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							sourceMap: false,
							url: false,
							minimize: false
						}
					},
					{
						loader: 'sass-loader',
						options: {
							importer: importOnce,
							includePaths: [
								path.resolve(__dirname, 'node_modules/compass-mixins/lib'),
								path.resolve(__dirname, 'node_modules'),
							],
							functions: {
								'inline-image($filename, $mime_type: null)': function(filename, mimeType, done) {
									done(sassInlineFile(filename, path.resolve(__dirname, 'src/assets/img')));
								}
							},
							sourceMap: false
						}
					}
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	]
};

function sassInlineFile(filename, rootDir) {
	let sourceFilePath = filename.getValue();
	let fullPath = path.resolve(rootDir, sourceFilePath.startsWith('/') ? sourceFilePath.slice(1) : sourceFilePath);
	let mimetype = mime.getType(path.extname(fullPath).slice(1));
	let fontContent = fs.readFileSync(fullPath);
	return new sass.types.String(`url(data:${mimetype ? `${mimetype};` : ''}base64,${fontContent.toString('base64')})`);
}