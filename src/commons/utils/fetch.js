let defaultConfig = {
	credentials: 'include',
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	}
};

export default function(input, config) {
	let configHeaders = Object.assign({}, defaultConfig.headers, config ? config.headers : {});
	let requestConfig = Object.assign({}, defaultConfig, config);
	requestConfig.headers = configHeaders;
	return fetch(input, requestConfig);
}