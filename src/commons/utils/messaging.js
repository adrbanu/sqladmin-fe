let listeners = {};

export default {
	subscribe,
	unsubscribe,
	send
};

function subscribe(name, callback) {
	listeners[name] = listeners[name] || [];
	if(!listeners[name].includes(callback)) listeners[name].push(callback);
}

function unsubscribe(name, callback) {
	listeners[name] = listeners[name].filter(listenerCallback => listenerCallback !== callback);
}

function send(name, data) {
	listeners[name] && listeners[name].forEach(listenerCallback => listenerCallback(data));
}