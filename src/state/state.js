import messaging from '../commons/utils/messaging';

let state = {
    view: {id: 'add-db-form'},
    dbs: [
        // {id: uniqueId(), name: 'Db1', tables: []},
        // {id: uniqueId(), name: 'Db2', tables: []},
        // {id: uniqueId(), name: 'Db3', tables: []},
        // {id: uniqueId(), name: 'Db4', tables: []}
    ]
};

export default {
    get,
    setView,
    addDb,
    addDbTable,
    addTables,
    removeTable,
    removeTables,
    addTableColumn,
    removeColumn,
};

function get() {
    return state;
}

function setView(view, payload) {
    state.view = {id: view, payload};
    messaging.send('state-changed');
}

function addDb(db) {
    state.dbs.push(db);
    messaging.send('state-changed');
}

function addTables(dbId, tables) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    db.tables = tables;
    messaging.send('state-changed');
}

function addDbTable(dbId, tableName, columns) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    db.tables = [...db.tables, {name: tableName, columns}];
    // db.tables.push({name: tableName, columns});
    messaging.send('state-changed');
}

function removeTables(dbId) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    db.tables = [];
    messaging.send('state-changed');
}

function removeTable(dbId, tableName) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    console.log(db);
    db.tables = db.tables.filter(table => table.name !== tableName);
    messaging.send('state-changed');
}

function removeColumn(dbId, tableName, columnName) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    let table = db.tables.find(table => table.name === tableName);
    if (!table) return;
    table.columns = table.columns.filter(column => column.name !== columnName);
    messaging.send('state-changed');
}

function addTableColumn(dbId, tableName, columnName) {
    let db = state.dbs.find(db => db.id === dbId);
    if (!db) return;
    let table = db.tables.find(table => table.name === tableName);
    if (!table) return;
    table.columns = [...table.columns, {name: columnName}];
    messaging.send('state-changed');
}