export default {
	getTables,
	getQueryResults
};

function getTables(dbId) {
	return [...Array(10).keys()].map(n => ({
		name: `Table ${n + 1} (db: ${dbId})`,
		columns: [...Array(6).keys()].map(m => ({
			name: `Column ${m + 1} (Table ${n + 1}, db: ${dbId})`
		}))
	}));
}

function getQueryResults(sentData) {
	return sentData.query ? getQueryLikeResults() : getTableLikeResults();
}

function getQueryLikeResults() {
	return {
		columns: [
			{name: 'ID'},
			{name: 'Title'},
			{name: 'Firstname'},
			{name: 'Lastname'},
			{name: 'Department'},
			{name: 'Salary'},
		],
		rows: [
			['1', 'Mr.', 'John', 'Doe', 'Sales', '48000'],
			['2', 'Mr.', 'Dick', 'Hunter', 'HR', '36000'],
			['3', 'Mrs.', 'Jane', 'Lumber', 'HR', '35000'],
			['4', 'Dr.', 'Bob', 'Jim', 'Orthopedy', '24000'],
			['5', 'Dr.', 'Hank', 'Rush', 'Cardiology', '27000'],
			['6', 'Dr.', 'Martin', 'Ica', 'Pediatry', '29000']
		]
	};
}

function getTableLikeResults() {
		return {
			columns: [
				{name: 'ID'},
				{name: 'Title'},
				{name: 'Firstname'},
				{name: 'Lastname'},
				{name: 'Age'},
				{name: 'Gender'},
				{name: 'Department'},
				{name: 'Salary'},
				{name: 'Hiring Date'}
			],
			rows: []
		};
	}