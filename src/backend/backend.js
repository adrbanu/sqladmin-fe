import fetchMock from 'fetch-mock';
import dataService from './data-service';
import uniqueId from '../commons/utils/unique-id';

let patterns = {
	addDb: /^\/databases\/add\/?(\?.*)?$/,
	tables: /^\/databases\/([0-9]+)\/tables\/?(\?.*)?$/,
	query: /^\/databases\/([0-9]+)\/query\/?(\?.*)?$/
};

fetchMock.post(patterns.addDb, (url, opts) => {
	let parsedData = JSON.parse(opts.body);
	return {status: 200, body: JSON.stringify({id: uniqueId(), name: parsedData.host})};
});

fetchMock.get(patterns.tables, (url, opts) => {
	let [, dbId] = url.match(patterns.tables);
	console.log({status: 200, body: JSON.stringify(dataService.getTables(dbId))});
	return {status: 200, body: JSON.stringify(dataService.getTables(dbId))};
});

fetchMock.post(patterns.query, (url, opts) => {
	let parsedData = JSON.parse(opts.body);
	return {status: 200, body: JSON.stringify(dataService.getQueryResults(parsedData))};
	// return {status: 409, body: JSON.stringify({message: 'Bad Query!'})};
});