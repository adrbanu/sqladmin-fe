import React, {Component} from 'react';
import fetch from '../../../commons/utils/fetch';
import state from '../../../state/state';

import './add-db-form.scss';

class AddDbForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: ''
        };

        this.addDb = this.addDb.bind(this);
    }

    addDb(host, schema, user, password) {

        if (!host || !schema || !user || !password) {
            this.setState(() => ({error: 'All fields are required.'}));
            return;
        }

        this.setState(() => ({error: ''}));
        this.hostEl = this.usernameEl = this.schemaEl = this.passwordEl = '';
        fetch('/api/databases/add', {
            method: 'POST',
            body: JSON.stringify({host, schema, user, password})
        })
            .then(r => r.json())
            .then(r => {
                state.addDb({
                    id: r.id,
                    name: r.name,
                    tables: []
                });
            });
    }

    render() {
        let {error} = this.state;
        return (
            <div className="add-db-form">
                <h1>Add Database</h1>
                <fieldset>
                    <span className="error">{error}</span>
                    <label>
                        <span>Host:</span>
                        <input ref={el => this.hostEl = el} type="text"/>
                    </label>
                    <label>
                        <span>Schema:</span>
                        <input ref={el => this.schemaEl = el} type="text"/>
                    </label>
                    <label>
                        <span>Username:</span>
                        <input ref={el => this.usernameEl = el} type="text"/>
                    </label>
                    <label>
                        <span>Password:</span>
                        <input ref={el => this.passwordEl = el} type="password"/>
                    </label>
                    <button
                        onClick={() => this.addDb(this.hostEl.value, this.schemaEl.value, this.usernameEl.value, this.passwordEl.value)}
                    >
                        Submit
                    </button>
                </fieldset>
            </div>
        );
    }
}

export default AddDbForm;