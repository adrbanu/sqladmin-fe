import React, {Component} from 'react';
import fetch from '../../../commons/utils/fetch';
import state from '../../../state/state';

import './add-column.scss';

let dataTypes = ["VARCHAR", "BIGINT", "BOOLEAN"];

class AddColumn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: ''
        };
    }

    addColumn(columnName, columnType) {
        let {db, table} = this.props;

        if (!columnName || !columnType) {
            this.setState(() => ({error: 'All fields are required.'}));
            return;
        }

        this.setState(() => ({error: ''}));
        fetch('/api/columns/add', {
            method: 'POST',
            body: JSON.stringify({databaseId: db.id, table: table.name, columnName: columnName, dataType: columnType})
        })
            .then(r => {
            state.addTableColumn(db.id, table.name, columnName);
        });
    }

    render() {
        let {error} = this.state;
        return (
            <div className="add-table">
                <h1>Add Table</h1>
                <fieldset>
                    <span className="error">{error}</span>
                    <label>
                        <span>Column Name:</span>
                        <input ref={el => this.columnNameEl = el} type="text"/>
                    </label>
                    <label>
                        <span>Data Type:</span>
                        <select ref={el => this.columnTypeEl = el}>
                            <option>Select type...</option>
                            {
                                dataTypes.map(type => (
                                    <option value={type}>{type}</option>
                                ))
                            }
                        </select>
                    </label>
                    <button
                        onClick={() => this.addColumn(this.columnNameEl.value, this.columnTypeEl.value)}>
                        Submit
                    </button>
                </fieldset>
            </div>
        );
    }
}

export default AddColumn;