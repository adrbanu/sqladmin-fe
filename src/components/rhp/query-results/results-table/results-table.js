import React, {Component} from 'react';

import './results-table.scss';

class ResultsTable extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let {columns, rows} = this.props;
		return (
			<div className="results-table">
				<table>
					<thead>
						<tr>
							{columns.map(col => (
								<th key={col.name}>{col.name}</th>
							))}
						</tr>
					</thead>
					<tbody>
						{rows.map((row, rowIndex) => (
							<tr key={rowIndex}>
								{row.map((value, valueIndex) => (
									<td key={valueIndex}>{value}</td>
								))}
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}

export default ResultsTable;