import React, {Component} from 'react';
import fetch from '../../../commons/utils/fetch';
import ResultsTable from './results-table/results-table';

import './query-results.scss';

class QueryResults extends Component {
    constructor(props) {
        super(props);

        this.state = {
            columns: [],
            rows: []
        };
    }

    runQuery(query) {
        let {db} = this.props;
        let dataToSend = {
            statement: query
        };
        fetch(`api/statements/${db.id}/run`, {
            method: 'POST',
            body: JSON.stringify(dataToSend)
        })
            .then(r => {
                if (r.ok) return r.json();
                else {
                    return r.json().then((err) => Promise.reject(err));
                }
            })
            .then(({queryResult}) => {
                console.warn(queryResult);
                queryResult.rows = queryResult.rows.map(qr => qr.values);
                if (queryResult) this.setState(() => queryResult);
            })
            .catch(error => alert(error.message));
    }

    render() {
        let {db} = this.props;
        let {columns, rows} = this.state;
        return (
            <div className="query-results">
                <h1>{`Run query for database ${db.name}.`}</h1>
                <textarea ref={el => this.textarea = el}></textarea>
                <button onClick={() => this.runQuery(this.textarea.value)}>Run Query</button>
                <ResultsTable columns={columns} rows={rows}/>
            </div>
        );
    }
}

export default QueryResults;