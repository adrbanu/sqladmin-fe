import React, {Component} from 'react';
import state from '../../state/state';
import AddDbForm from './add-db-form/add-db-form';
import QueryResults from './query-results/query-results';
import AddTable from './add-table/add-table';
import AddColumn from './add-column/add-column';

import './rhp.scss';

class RHP extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let view = state.get().view;
		return (
			<div className="rhp">
				{view.id === 'add-db-form' && (
					<AddDbForm />
				)}
				{view.id === 'query-results' && (
					<QueryResults db={view.payload.db}/>
				)}
                {view.id === 'add-table' && (
                    <AddTable db={view.payload.db}/>
                )}
                {view.id === 'add-column' && (
                    <AddColumn db={view.payload.db} table={view.payload.table}/>
                )}
			</div>
		);
	}
}

export default RHP;