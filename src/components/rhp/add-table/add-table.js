import React, {Component} from 'react';
import fetch from '../../../commons/utils/fetch';
import state from '../../../state/state';

import './add-table.scss';

class AddTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: ''
        };
    }

    addTable(tableName) {
        let {db} = this.props;

        if (!tableName) {
            this.setState(() => ({error: 'All fields are required.'}));
            return;
        }

        this.setState(() => ({error: ''}));
        fetch('/api/tables/add', {
            method: 'POST',
            body: JSON.stringify({databaseId: db.id, name: tableName, columns: []})
        })
            .then(r => {
                state.addDbTable(db.id, tableName, []);
            });
    }

    render() {
        let {error} = this.state;
        return (
            <div className="add-table">
                <h1>Add Table</h1>
                <fieldset>
                    <span className="error">{error}</span>
                    <label>
                        <span>Table Name:</span>
                        <input ref={el => this.tableNameEl = el} type="text"/>
                    </label>
                    <button
                        onClick={() => this.addTable(this.tableNameEl.value)}
                    >
                        Submit
                    </button>
                </fieldset>
            </div>
        );
    }
}

export default AddTable;