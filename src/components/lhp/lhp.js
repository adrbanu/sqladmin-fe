import React, {Component} from 'react';
import AddDbButton from './add-db-button/add-db-button';
import DbList from './db-list/db-list';

import './lhp.scss';

class LHP extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="lhp">
				<AddDbButton />
				<DbList />
			</div>
		);
	}
}

export default LHP;