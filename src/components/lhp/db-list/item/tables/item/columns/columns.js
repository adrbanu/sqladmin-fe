import React, {Component} from 'react';

import './columns.scss';
import state from "../../../../../../../state/state";
import fetch from "../../../../../../../commons/utils/fetch";

class Columns extends Component {
	constructor(props) {
		super(props);
	}

    onAddColumn() {
        let {db, table} = this.props;
        state.setView("add-column", {db, table});
    }

    onDeleteColumn(column) {
        let {db, table} = this.props;
        fetch(`/api/columns/drop`, {
            method: "DELETE",
            body: JSON.stringify({databaseId: db.id, table: table.name, columnName: column.name})
        })
            .then(() => {
                state.removeColumn(db.id, table.name, column.name);
            })
    }

	render() {
		let {table} = this.props;
		return (
			<div className="columns">
				{table.columns.map(col => (
					<div key={col.name}>
						<span className="column-name">{col.name}</span>
						<span className="icon-remove" onClick={() => this.onDeleteColumn(col)}></span>
						</div>
				))}
                <span className="add-column-icon"/>
                <span className="add-column" onClick={() => this.onAddColumn(table.name)}>Add Column</span>
			</div>
		);
	}
}

export default Columns;