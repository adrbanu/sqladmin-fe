import React, {Component} from 'react';
import classnames from 'classnames';
import Columns from './columns/columns';
import state from '../../../../../../state/state';
import fetch from '../../../../../../commons/utils/fetch';

import './item.scss';

class TableItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false
        };
    }

    onDeleteTable() {
        let {db, table} = this.props;
        fetch(`/api/tables/drop`, {
            method: "DELETE",
            body: JSON.stringify({databaseId: db.id, tableName: table.name})
        })
            .then(() => {
                state.removeTable(db.id, table.name);
            })
    }

    render() {
        let {expanded} = this.state;
        let {db, table} = this.props;
        return (
            <div className="table-item">
				<span
                    className={classnames({
                        'icon-plus': !expanded,
                        'icon-minus': expanded
                    })}
                    onClick={() => this.setState(() => ({expanded: !expanded}))}>
				</span>
                <span className="table-name">{table.name}</span>
                <span className="icon-remove" onClick={() => this.onDeleteTable(table.name)}></span>
                {expanded && (
                    <Columns key={table.name} db={db} table={table}/>
                )}
            </div>
        );
    }
}

export default TableItem;