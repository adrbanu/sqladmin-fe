import React, {Component} from 'react';
import TableItem from './item/item';
import state from "../../../../../state/state";

import './tables.scss';

class Tables extends Component {
    constructor(props) {
        super(props);
    }

    onTableAdd() {
        let {db} = this.props;
        state.setView('add-table', {db})
    }

    render() {
        let {db} = this.props;
        return (
            <div className="tables">
                {db.tables.map(table => (
                    <TableItem key={table.name} db={db} table={table}/>
                ))}
                {db.tables.length !== 0 &&
                <div className="table-add">
                    <span className="table-add-icon"/>
                    <span className="table-add-text" onClick={() => this.onTableAdd(db.id)}>Add Table</span>
                </div>
                }
            </div>
        );
    }
}

export default Tables;