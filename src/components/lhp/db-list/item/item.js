import React, {Component} from 'react';
import classnames from 'classnames';
import state from '../../../../state/state';
import fetch from '../../../../commons/utils/fetch';
import Tables from './tables/tables';

import './item.scss';

class DbListItem extends Component {
    constructor(props) {
        super(props);

        this.toggleExpandCollapse = this.toggleExpandCollapse.bind(this);
        this.onDatabaseNameClick = this.onDatabaseNameClick.bind(this);
    }

    onDatabaseNameClick() {
        let {db} = this.props;
        state.setView('query-results', {db});
    }

    toggleExpandCollapse() {
        let {db} = this.props;
        if (db.tables.length) {
            state.removeTables(db.id);
        } else {
            fetch(`/api/tables/all/${db.id}`)
                .then(r => {
                    if (r.ok) return r.json();
                    else {
                        return r.json().then((err) => Promise.reject(err));
                    }
                })
                .then(function ({tables}) {
                    state.addTables(db.id, tables);
                })
                .catch((error) => {
                    console.log(error);
                    console.log("jahahhajsdha");
                    alert(error.message)
                });
        }
    }

    render() {
        let {db} = this.props;
        return (
            <div className="db-list-item">
				<span
                    className={classnames({
                        'icon-plus': db.tables.length === 0,
                        'icon-minus': db.tables.length > 0
                    })}
                    onClick={this.toggleExpandCollapse}>
				</span>
                <span className="db-name" onClick={this.onDatabaseNameClick}>{db.name}</span>
                <Tables db={db}></Tables>
            </div>
        );
    }
}

export default DbListItem;