import React, {Component} from 'react';
import DbListItem from './item/item';
import state from '../../../state/state';
import fetch from '../../../commons/utils/fetch';

class DbList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        fetch(`/api/databases/all/4`)
            .then(r => r.json())
            .then(function ({databases}) {
                console.log(databases);
                databases.forEach((db) => {
                    state.addDb(Object.assign({tables: []}, db));
                })
            });
    }

    render() {
        let dbs = state.get().dbs;
        return (
            <div className="db-list">
                {dbs.map(db => (
                    <DbListItem key={db.id} db={db}/>
                ))}
            </div>
        );
    }
}

export default DbList;