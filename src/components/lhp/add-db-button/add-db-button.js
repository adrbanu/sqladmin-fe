import React, {Component} from 'react';
import state from '../../../state/state';

import './add-db-button.scss';

class AddDbButton extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="add-db-button" onClick={() => state.setView('add-db-form')}>ADD</div>
		);
	}
}

export default AddDbButton;