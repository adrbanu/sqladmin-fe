import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import messaging from './commons/utils/messaging';
import Lhp from './components/lhp/lhp.js';
import Rhp from './components/rhp/rhp.js';

import './index.scss';

class App extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		messaging.subscribe('state-changed', () => this.forceUpdate());
	}

	render() {
		return (
			<Fragment>
				<header>SqlAdmin</header>
				<Lhp></Lhp>
				<Rhp></Rhp>
			</Fragment>
		);
	}
}

ReactDOM.render(<App />, document.querySelector('#app'));